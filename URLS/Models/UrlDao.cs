﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace URLS.Models
{
    public class UrlDao
    {

        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AppNameCString"].ConnectionString;

        public void AddUrlToDB(UrlModel model)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand command = new SqlCommand("", sqlConnection);

                command.CommandText = "insert into tblUrl(fldUrlId,fldOriginalUrl) values(@fldUrlId,@fldOriginalUrl)";
                command.Parameters.AddWithValue("@fldUrlId", model.fldUrlId);
                command.Parameters.AddWithValue("@fldOriginalUrl", model.fldOriginalUrl);
                command.ExecuteNonQuery();
                sqlConnection.Close();
            }
        }

        public string GetUrlFromDB(string shortUrl)
        {
            string record = "";
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand command = new SqlCommand("", sqlConnection);

                command.CommandText = "SELECT TOP 1 * FROM tblUrl WHERE fldUrlId = @fldUrlId";
                command.Parameters.AddWithValue("@fldUrlId", shortUrl);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    record = reader["fldOriginalUrl"].ToString();
                }
                sqlConnection.Close();
            }

            return record;
        }

    }
}