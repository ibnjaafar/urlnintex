﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URLS.Models
{
    public class UrlModel
    {
        public int fldUrlId { get; set; }
        public string fldOriginalUrl { get; set; }
    }
}