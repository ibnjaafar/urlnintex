﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URLS.Models
{
    public class UrlService
    {

        public static readonly string Alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
        public static readonly int Base = Alphabet.Length;

        public static string Encode(int i)
        {
            if (i == 0) return Alphabet[0].ToString();

            var s = string.Empty;

            while (i > 0)
            {
                s += Alphabet[i % Base];
                i = i / Base;
            }

            return string.Join(string.Empty, s.Reverse());
        }

        public static int convertToID(string s)
        {
            var i = 0;

            foreach (var c in s)
            {
                i = (i * Base) + Alphabet.IndexOf(c);
            }
            
            return i;
        }

        public static int shortenUrl(string url)
        {
            string result = "";
            // Simple test of encode/decode operations

            foreach (char c in url)
            {
                result += Encode(c);
            }

            return convertToID(result);
        }
    }
}