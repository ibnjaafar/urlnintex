﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using URLS.Models;

namespace URLS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ShortUrl(FormCollection collection)
        {
            UrlModel urlmodel = new UrlModel();
            UrlDao urlDao = new UrlDao();
            string originalUrl = collection["originalUrl"];
            int shortenedUrl = UrlService.shortenUrl(collection["originalUrl"]);
            var currentURL = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;

            urlmodel.fldOriginalUrl = originalUrl;
            urlmodel.fldUrlId = shortenedUrl;

            //Add url to database
            urlDao.AddUrlToDB(urlmodel);
            
            ViewBag.ShortUrl = currentURL +"/r/" + shortenedUrl;
            ViewBag.OriginalUrl = originalUrl;

            return View();
        }

        [HttpPost]
        public ActionResult OriginalUrl(FormCollection collection)
        {
            UrlDao urlDao = new UrlDao();

            var currentURL = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;
            //Trim domain name
            string shortenedUrl = collection["shortUrl"].Replace(currentURL + "/r/", "");
            //Get URL From Database
            string originalUrl = urlDao.GetUrlFromDB(shortenedUrl);

            if (!originalUrl.Equals(""))
            {
                ViewBag.ShortUrl = currentURL + "/r/" + shortenedUrl;
                ViewBag.OriginalUrl = originalUrl;
            }
            else {
                ViewBag.NoData = "No URL stored in this system. Please shorten it first";
            }

            return View();
        }

        

    }
}