﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using URLS.Models;

namespace URLS.Controllers
{
    public class RedirectController : Controller
    {
        // GET: Redirect
        public ActionResult Index(string id)
        {
            UrlDao urlDao = new UrlDao();

            if (id == "")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                string shortenedUrl = id;
                //Get URL From Database
                string originalUrl = urlDao.GetUrlFromDB(shortenedUrl);

                if (!originalUrl.Equals(""))
                {
                    originalUrl.Replace("http://","");
                    originalUrl.Replace("https://", "");
                    ViewBag.RedirectUrl = originalUrl;
                }
                else
                {
                    ViewBag.NoData = "No URL stored in this system. Please shorten it first";
                }

                return View();
            }
        }
    }
}